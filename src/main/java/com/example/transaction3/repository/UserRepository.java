package com.example.transaction3.repository;

import com.example.transaction3.entity.User;
import com.example.transaction3.entity.enums.PermissionEnum;
import lombok.NonNull;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;
import java.util.UUID;
@Repository
public interface UserRepository extends JpaRepository<User, UUID> {
    Optional<User> findByPhoneNumber(String phoneNumber);

    boolean existsById(@NonNull UUID id);

    @Transactional
    @Modifying
    @Query(value = "UPDATE User SET role = :insteadOfRoleId WHERE role = :id")
    void updateRole(Integer id, Integer insteadOfRoleId);

    boolean existsByPhoneNumber(String phoneNumber);
}

