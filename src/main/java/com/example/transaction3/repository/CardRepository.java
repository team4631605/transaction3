package com.example.transaction3.repository;

import com.example.transaction3.entity.Card;
import com.example.transaction3.entity.enums.CardTypeEnum;
import org.springframework.data.domain.Example;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import javax.validation.constraints.Size;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Repository
public interface CardRepository extends JpaRepository<Card,Long> {

    boolean existsByCardNumber(@Size(min = 16, max = 16) String cardNumber);
    boolean existsByCardNumberAndUserId(@Size(min = 16, max = 16) String cardNumber, UUID user_id);

    List<Card> findAllByUserIdAndDeletedFalse(UUID user_id);

    Optional<Card> findByCardNumber(String cardNumber);

}
