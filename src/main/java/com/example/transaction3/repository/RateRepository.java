package com.example.transaction3.repository;

import com.example.transaction3.entity.Rate;
import com.example.transaction3.entity.enums.CurrencyEnum;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface RateRepository extends JpaRepository<Rate,Long> {

    Optional<Rate> findByFromCurrencyAndToCurrency(CurrencyEnum fromCurrency, CurrencyEnum toCurrency);
}
