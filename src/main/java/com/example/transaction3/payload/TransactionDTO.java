package com.example.transaction3.payload;

import com.example.transaction3.entity.enums.CurrencyEnum;
import com.example.transaction3.utils.MessageConstants;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotNull;
import java.util.UUID;

@Getter
@Setter
@Builder
public class TransactionDTO {
    private UUID id;
    private Long senderCardId;
    private Long senderAmount;
    private Long receiverCardId;
    private Long receiverAmount;
    private CurrencyEnum senderCurrency;
    private CurrencyEnum receiverCurrency;

}
