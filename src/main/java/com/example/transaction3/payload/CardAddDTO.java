package com.example.transaction3.payload;

import com.example.transaction3.entity.enums.CardTypeEnum;
import com.example.transaction3.utils.MessageConstants;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
public class CardAddDTO {
    @NotNull(message = MessageConstants.CARD_BALANCE_MUST_NOT_BE_NULL)
    @Min(value = 0,message = "Balance must not be negative")
    private Long balance;
    @NotBlank(message = MessageConstants.CARD_NAME_MUST_NOT_BE_BLANK)
    private String name;
    @Size(min = 16,max = 16, message = "At least must be 16 numbers")
    @NotBlank(message = MessageConstants.EMPTY_FIELD)
    private String cardNumber;
}
