package com.example.transaction3.payload;

import com.example.transaction3.utils.MessageConstants;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.validation.constraints.NotNull;
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class TransactionHoldDTO {
    @NotNull(message = MessageConstants.SENDER_CARD_ID_MUST_NOT_BE_NULL)
    private Long senderCardId;
    @NotNull(message = MessageConstants.RECEIVER_CARD_ID_MUST_NOT_BE_NULL)
    private Long receiverCardId;
    @NotNull(message = MessageConstants.SENDER_AMOUNT_MUST_NOT_BE_NULL)
    private Long amount;
}
