package com.example.transaction3.payload;

import com.example.transaction3.entity.enums.CardTypeEnum;
import com.example.transaction3.entity.enums.CurrencyEnum;
import com.example.transaction3.utils.MessageConstants;
import lombok.*;

import javax.validation.constraints.NotBlank;
import java.util.UUID;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@Builder
public class CardDTO {

    private Long id;
    private Long balance;
    private String name;
    private String cardNumber;
    private CardTypeEnum cardType;
    private CurrencyEnum currency;
    private UUID userId;

}
