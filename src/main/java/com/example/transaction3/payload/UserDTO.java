package com.example.transaction3.payload;

import com.example.transaction3.entity.User;
import com.example.transaction3.entity.enums.PermissionEnum;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.*;

import java.util.Set;
import java.util.UUID;
import java.util.stream.Collectors;

@NoArgsConstructor
@Getter
@Setter
@JsonInclude(JsonInclude.Include.NON_NULL)
@AllArgsConstructor
@Builder
public class UserDTO {

    private UUID id;

    private String phoneNumber;

    private boolean enabled;

    private Set<PermissionEnum> permissions;

    private Integer roleId;

    private String roleName;

    public static UserDTO mapUserDTO(User user) {
        return UserDTO.builder()
                .phoneNumber(user.getPhoneNumber())
                .permissions(user.getRole().getPermissions())
                .enabled(user.isEnabled())
                .id(user.getId())
                .roleId(user.getRole().getId())
                .roleName(user.getRole().getName())
                .build();
    }

    public static Set<UserDTO> mapUserDTO(Set<User> users) {
        return users.stream().map(UserDTO::mapUserDTO).collect(Collectors.toSet());
    }


}

