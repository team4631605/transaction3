package com.example.transaction3.payload;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Pattern;

import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

import static com.example.transaction3.utils.MessageConstants.EMPTY_FIELD;
import static com.example.transaction3.utils.MessageConstants.MISMATCH;

@Getter
@AllArgsConstructor
@NoArgsConstructor
public class SignDTO {

    @ApiModelProperty(notes = "Phone number of the user. Note that enter only valid Uzbekistan's numbers",
            required = true)
    @NotBlank(message = EMPTY_FIELD)
    @Pattern(regexp = "^([+]?\\d{3}[-\\s]?|)\\d{2}[-\\s]?\\d{3}[-\\s]?\\d{2}[-\\s]?\\d{2}$", message = MISMATCH)
    private String phoneNumber;

    @ApiModelProperty(notes = "Password of the user. " +
            "Note that password must contains at least 1 upper, 1 lower and 1 numeric character")
    @NotBlank(message = EMPTY_FIELD)
    private String password;
}
