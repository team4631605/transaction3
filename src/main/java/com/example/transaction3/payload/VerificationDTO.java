package com.example.transaction3.payload;

import io.swagger.annotations.ApiModelProperty;
import lombok.Builder;
import lombok.Getter;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

import static com.example.transaction3.utils.MessageConstants.EMPTY_FIELD;
import static com.example.transaction3.utils.MessageConstants.MISMATCH;


@Builder
@Getter
public class VerificationDTO {
    @Pattern(regexp = "^([+]?\\d{3}[-\\s]?|)\\d{2}[-\\s]?\\d{3}[-\\s]?\\d{2}[-\\s]?\\d{2}$", message = MISMATCH)
    @ApiModelProperty(notes = "Phone number of the user. Note that enter only valid Uzbekistan's numbers",
            required = true)
    @NotBlank(message = EMPTY_FIELD)
    private String phoneNumber;

    @ApiModelProperty(notes = "Verification code of the user. " )
    @Size(min = 8,max = 8, message = "At least must be 8 characters")
    @NotBlank(message = EMPTY_FIELD)
    private String verificationCode;
}
