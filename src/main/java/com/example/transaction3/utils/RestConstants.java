package com.example.transaction3.utils;

import com.example.transaction3.controller.AuthController;
import com.example.transaction3.controller.CardController;
import com.fasterxml.jackson.databind.ObjectMapper;

public interface RestConstants {
    ObjectMapper objectMapper = new ObjectMapper();

    String AUTHENTICATION_HEADER = "Authorization";

    String AUTHORIZATION_HEADER = "Authorization";

    String REQUEST_ATTRIBUTE_CURRENT_USER = "User";
    String REQUEST_ATTRIBUTE_CURRENT_USER_ID = "UserId";
    String REQUEST_ATTRIBUTE_CURRENT_USER_PERMISSIONS = "Permissions";

    String[] OPEN_PAGES = {
            "/*",
            AuthController.AUTH_CONTROLLER_BASE_PATH + "/**",
            CardController.CARD_CONTROLLER_BASE_PATH + CardController.GET_PATH,
    };
    String BASE_PATH = "/api/v1/";







}
