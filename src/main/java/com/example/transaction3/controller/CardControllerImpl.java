package com.example.transaction3.controller;

import com.example.transaction3.payload.ApiResult;
import com.example.transaction3.payload.CardAddDTO;
import com.example.transaction3.payload.CardDTO;
import com.example.transaction3.payload.VerificationDTO;
import com.example.transaction3.service.CardService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequiredArgsConstructor
public class CardControllerImpl implements CardController {

    private final CardService cardService;

    @Override
    public ApiResult<CardDTO> add(CardAddDTO cardDTO) {
        return cardService.add(cardDTO);
    }

    @Override
    public ApiResult<List<CardDTO>> getAll() {
        return cardService.getAll();
    }

    @Override
    public ApiResult<CardDTO> get(String cardNumber) {
        return cardService.get(cardNumber);
    }
}
