package com.example.transaction3.controller;

import com.example.transaction3.payload.ApiResult;
import com.example.transaction3.payload.SignDTO;
import com.example.transaction3.payload.TokenDTO;
import com.example.transaction3.payload.VerificationDTO;
import com.example.transaction3.utils.RestConstants;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RequestMapping(path = AuthController.AUTH_CONTROLLER_BASE_PATH)
public interface AuthController {
    String AUTH_CONTROLLER_BASE_PATH = RestConstants.BASE_PATH + "auth";
    String SIGN_UP_PATH = "/sign-up";
    String SIGN_IN_PATH = "/sign-in";
    String DELETE_PATH = "/delete";
    String REFRESH_TOKEN_PATH = "/refresh-token";
    String VERIFICATION_PATH = "/verification-phone-number";


    @PostMapping(path = SIGN_UP_PATH)
    ApiResult<VerificationDTO> signUp(@Valid @RequestBody SignDTO signDTO);
    @PostMapping(path = SIGN_IN_PATH)
    ApiResult<TokenDTO> signIn(@Valid @RequestBody SignDTO signDTO);

    @DeleteMapping(path = DELETE_PATH)
    ApiResult<?> delete();

    @PostMapping(value = VERIFICATION_PATH)
    ApiResult<?> verificationPhoneNumber(@Valid @RequestBody VerificationDTO verificationDTO);

    @GetMapping(path = REFRESH_TOKEN_PATH)
    ApiResult<TokenDTO> refreshToken(@RequestHeader(value = "Authorization") String accessToken,
                                     @RequestHeader(value = "RefreshToken") String refreshToken);

}
