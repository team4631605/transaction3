package com.example.transaction3.controller;

import com.example.transaction3.aop.annotation.CheckAuth;
import com.example.transaction3.entity.enums.PermissionEnum;
import com.example.transaction3.payload.*;
import com.example.transaction3.utils.RestConstants;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RequestMapping(CardController.CARD_CONTROLLER_BASE_PATH)
public interface CardController {
    String CARD_CONTROLLER_BASE_PATH = RestConstants.BASE_PATH + "card";
    String ADD_PATH = "/add";
    String GET_ALL_PATH = "/my-cards";
    String GET_PATH = "/card-info/{cardNumber}";


    @PostMapping(path = ADD_PATH)
    @CheckAuth(permission = PermissionEnum.ADD_CARD)
    ApiResult<CardDTO> add(@Valid @RequestBody CardAddDTO cardDTO);

    @GetMapping(GET_ALL_PATH)
    @CheckAuth(permission = PermissionEnum.GET_ALL_USER_CARDS)
    ApiResult<List<CardDTO>> getAll();

    @GetMapping(value = GET_PATH)
    ApiResult<CardDTO> get(@PathVariable String cardNumber);


}
