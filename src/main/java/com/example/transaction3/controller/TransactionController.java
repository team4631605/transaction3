package com.example.transaction3.controller;

import com.example.transaction3.aop.annotation.CheckAuth;
import com.example.transaction3.entity.enums.PermissionEnum;
import com.example.transaction3.payload.ApiResult;
import com.example.transaction3.payload.TransactionDTO;
import com.example.transaction3.payload.TransactionHoldDTO;
import com.example.transaction3.utils.RestConstants;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.validation.Valid;
import java.util.UUID;

@RequestMapping(TransactionController.TRANSACTION_CONTROLLER_BASE_PATH)
public interface TransactionController {
    String TRANSACTION_CONTROLLER_BASE_PATH = RestConstants.BASE_PATH + "transaction";
    String HOLD_PATH = "/hold";
    String CONFIRM_PATH = "/confirm/{transaction_id}";

    @PostMapping(path = HOLD_PATH)
    @CheckAuth(permission = PermissionEnum.HOLD_TRANSACTION)
    ApiResult<TransactionDTO> hold(@Valid @RequestBody TransactionHoldDTO transactionHoldDTO);
    @PostMapping(path = CONFIRM_PATH)
    @CheckAuth(permission = PermissionEnum.CONFIRM_TRANSACTION)
    ApiResult<?> confirm(@PathVariable UUID transaction_id);

}
