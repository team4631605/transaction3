package com.example.transaction3.controller;

import com.example.transaction3.payload.ApiResult;
import com.example.transaction3.payload.SignDTO;
import com.example.transaction3.payload.TokenDTO;
import com.example.transaction3.payload.VerificationDTO;
import com.example.transaction3.service.AuthService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

@RequiredArgsConstructor
@RestController
@Slf4j
public class AuthControllerImpl implements AuthController {
    private final AuthService authService;

    @Override
    public ApiResult<VerificationDTO> signUp(SignDTO signDTO) {
        return authService.signUp(signDTO);
    }

    @Override
    public ApiResult<TokenDTO> signIn(@Valid SignDTO signDTO) {
        return authService.signIn(signDTO);
    }

    @Override
    public ApiResult<?> delete() {
        return authService.delete();
    }

    public ApiResult<?> verificationPhoneNumber(@Valid @RequestBody VerificationDTO verificationDTO) {
        return authService.verificationPhoneNumber(verificationDTO);
    }

    @Override
    public ApiResult<TokenDTO> refreshToken(String accessToken, String refreshToken) {
        return authService.refreshToken(accessToken, refreshToken);
    }


}
