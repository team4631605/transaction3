package com.example.transaction3.controller;

import com.example.transaction3.payload.ApiResult;
import com.example.transaction3.payload.TransactionDTO;
import com.example.transaction3.payload.TransactionHoldDTO;
import com.example.transaction3.service.TransactionService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.RestController;

import java.util.UUID;
@RestController
@RequiredArgsConstructor
public class TransactionControllerImpl implements TransactionController {

    private final TransactionService transactionService;

    @Override
    public ApiResult<TransactionDTO> hold(TransactionHoldDTO transactionHoldDTO) {
        return transactionService.hold(transactionHoldDTO);
    }

    @Override
    public ApiResult<?> confirm(UUID transaction_id) {
        return transactionService.confirm(transaction_id);
    }
}
