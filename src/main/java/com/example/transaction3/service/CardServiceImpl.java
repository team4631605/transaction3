package com.example.transaction3.service;

import com.example.transaction3.entity.Card;
import com.example.transaction3.entity.User;
import com.example.transaction3.entity.enums.CardTypeEnum;
import com.example.transaction3.entity.enums.CurrencyEnum;
import com.example.transaction3.exceptions.RestException;
import com.example.transaction3.payload.ApiResult;
import com.example.transaction3.payload.CardAddDTO;
import com.example.transaction3.payload.CardDTO;
import com.example.transaction3.payload.ErrorData;
import com.example.transaction3.repository.CardRepository;
import com.example.transaction3.utils.CommonUtils;
import com.example.transaction3.utils.MessageConstants;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class CardServiceImpl implements CardService {

    private final CardRepository cardRepository;
    private final ValidatorService validator;

    @Override
    public ApiResult<CardDTO> add(CardAddDTO cardDTO) {

        List<ErrorData> errorData = validator.checkCardNumberIsValidOrThrew(cardDTO.getCardNumber());
        if (!errorData.isEmpty())
            throw RestException.restThrow(errorData,HttpStatus.BAD_REQUEST);

        if (cardRepository.existsByCardNumber(cardDTO.getCardNumber()))
            throw RestException.restThrow(MessageConstants.CARD_ALREADY_EXISTS, HttpStatus.BAD_REQUEST);

        Card card = cardRepository.save(mapCardDtoToCard(cardDTO));

        return ApiResult.successResponse(mapCardToCardDTO(card));
    }


    @Override
    public ApiResult<List<CardDTO>> getAll() {
        User currentUser = CommonUtils.getCurrentUser();
        List<Card> cards = cardRepository.findAllByUserIdAndDeletedFalse(currentUser.getId());

        return ApiResult.successResponse(cards.stream().map(this::mapCardToCardDTO)
                .collect(Collectors.toList()));
    }

    @Override
    public ApiResult<CardDTO> get(String cardNumber) {
        User currentUser = CommonUtils.getCurrentUser();

        List<ErrorData> errorData = validator.checkCardNumberIsValidOrThrew(cardNumber);
        if (!errorData.isEmpty())
            throw RestException.restThrow(errorData,HttpStatus.BAD_REQUEST);

        Card card = cardRepository.findByCardNumber(cardNumber).orElseThrow(() ->
                RestException.restThrow(MessageConstants.CARD_NOT_FOUND, HttpStatus.NOT_FOUND));

//        if (!Objects.equals(card.getUser().getId() , currentUser.getId()))
//            throw RestException.restThrow(MessageConstants.NOT_ACCESS_TO_CARD, HttpStatus.FORBIDDEN);

        return ApiResult.successResponse(mapCardToCardDTO(card));
    }


    private CardDTO mapCardToCardDTO(Card card) {
        return CardDTO.builder()
                .id(card.getId())
                .name(card.getName())
                .cardNumber(card.getCardNumber())
                .balance(card.getBalance())
                .userId(card.getUser().getId())
                .cardType(card.getType())
                .currency(card.getCurrency())
                .build();
    }

    private CurrencyEnum findCardCurrency(CardTypeEnum cardTypeEnum) {
        if (Objects.equals(cardTypeEnum,CardTypeEnum.HUMO))
            return CurrencyEnum.SUM;
        return CurrencyEnum.USD;
    }

    private CardTypeEnum findCardType(String cardNumber) {
        if (cardNumber.startsWith("9860"))
            return CardTypeEnum.HUMO;
        return CardTypeEnum.VISA;
    }


    private Card mapCardDtoToCard(CardAddDTO cardDTO ) {
        User currentUser = CommonUtils.getCurrentUser();
        CardTypeEnum cardType = findCardType(cardDTO.getCardNumber());
        CurrencyEnum cardCurrency = findCardCurrency(cardType);

        return Card.builder()
                .balance(cardDTO.getBalance())
                .name(cardDTO.getName())
                .user(currentUser)
                .type(cardType)
                .currency(cardCurrency)
                .cardNumber(cardDTO.getCardNumber())
                .build();
    }

}
