package com.example.transaction3.service;

import com.example.transaction3.entity.User;
import com.example.transaction3.payload.ApiResult;
import com.example.transaction3.payload.SignDTO;
import com.example.transaction3.payload.TokenDTO;
import com.example.transaction3.payload.VerificationDTO;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.web.bind.annotation.RequestBody;


import javax.validation.Valid;
import java.util.Optional;
import java.util.UUID;

public interface AuthService extends UserDetailsService {
    Optional<User> getUserById(UUID id);

    ApiResult<TokenDTO> signIn(SignDTO signDTO);

    ApiResult<TokenDTO> refreshToken(String accessToken, String refreshToken);

    ApiResult<VerificationDTO> signUp(SignDTO signDTO);

    ApiResult<?> verificationPhoneNumber(VerificationDTO verificationDTO);

    ApiResult<?> delete();

}
