package com.example.transaction3.service;

import com.example.transaction3.payload.ApiResult;
import com.example.transaction3.payload.TransactionDTO;
import com.example.transaction3.payload.TransactionHoldDTO;

import java.util.UUID;

public interface TransactionService {
    ApiResult<TransactionDTO> hold(TransactionHoldDTO transactionHoldDTO);
    ApiResult<?> confirm(UUID transactionId);
}
