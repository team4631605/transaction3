package com.example.transaction3.service;

import com.example.transaction3.entity.Role;
import com.example.transaction3.entity.User;
import com.example.transaction3.entity.enums.RoleEnum;
import com.example.transaction3.exceptions.RestException;
import com.example.transaction3.payload.*;
import com.example.transaction3.repository.CardRepository;
import com.example.transaction3.repository.RoleRepository;
import com.example.transaction3.repository.UserRepository;
import com.example.transaction3.security.JwtTokenProvider;
import com.example.transaction3.utils.CommonUtils;
import com.example.transaction3.utils.MessageConstants;
import com.example.transaction3.utils.StringHelper;
import io.jsonwebtoken.ExpiredJwtException;
import org.springframework.context.annotation.Lazy;
import org.springframework.http.HttpStatus;
import org.springframework.security.authentication.*;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Service
public class AuthServiceImpl implements AuthService {

    private final JwtTokenProvider jwtTokenProvider;
    private final AuthenticationManager authenticationManager;
    private final UserRepository userRepository;
    private final CardRepository cardRepository;
    private final PasswordEncoder passwordEncoder;
    private final RoleRepository roleRepository;
    private final ValidatorService validator;

    public AuthServiceImpl(JwtTokenProvider jwtTokenProvider,
                           @Lazy AuthenticationManager authenticationManager,
                           PasswordEncoder passwordEncoder,
                           UserRepository userRepository,
                           CardRepository cardRepository,
                           RoleRepository roleRepository, ValidatorService validator) {
        this.jwtTokenProvider = jwtTokenProvider;
        this.authenticationManager = authenticationManager;
        this.userRepository = userRepository;
        this.passwordEncoder = passwordEncoder;
        this.cardRepository = cardRepository;
        this.roleRepository = roleRepository;
        this.validator = validator;
    }

    @Override
    public UserDetails loadUserByUsername(String phoneNumber) throws UsernameNotFoundException {
        return userRepository
                .findByPhoneNumber(phoneNumber)
                .orElseThrow(() -> new UsernameNotFoundException(phoneNumber));
    }

    @Override
    public ApiResult<VerificationDTO> signUp(SignDTO signDTO) {
        List<ErrorData> errors = validator.validateUser(signDTO);
        if (!errors.isEmpty())
            throw RestException.restThrow(errors,HttpStatus.BAD_REQUEST) ;

        User user = new User(
                signDTO.getPhoneNumber(),
                passwordEncoder.encode(signDTO.getPassword()));

        Role role = roleRepository.findByName(RoleEnum.USER.name())
                .orElseThrow(() -> RestException.restThrow(MessageConstants.ROLE_NOT_FOUND));
        user.setRole(role);

        String verificationCode = StringHelper.generateVerificationCode();
        user.setVerificationCode(verificationCode);
        user = userRepository.save(user);
        return ApiResult.successResponse(VerificationDTO.builder()
                .phoneNumber(user.getPhoneNumber())
                .verificationCode(verificationCode).build());
    }

    @Override
    public ApiResult<?> verificationPhoneNumber(VerificationDTO verificationDTO) {
        List<ErrorData> errors = validator.validateVerificationCode(verificationDTO);
        if (!errors.isEmpty())
            throw RestException.restThrow(errors,HttpStatus.BAD_REQUEST) ;

        return ApiResult.successResponse(MessageConstants.SUCCESSFULLY_VERIFIED);
    }

    @Override
    public ApiResult<?> delete() {
        User currentUser = CommonUtils.getCurrentUser();
        userRepository.delete(currentUser);
        return ApiResult.successResponse(MessageConstants.SUCCESSFULLY_DELETED);
    }

    @Override
    public ApiResult<TokenDTO> signIn(SignDTO signInForEmployeeDTO) {
        User user;
        try {
            Authentication authentication = authenticationManager.authenticate(
                    new UsernamePasswordAuthenticationToken(
                            signInForEmployeeDTO.getPhoneNumber(),
                            signInForEmployeeDTO.getPassword()
                    ));
            SecurityContextHolder.getContext().setAuthentication(authentication);
            user = (User) authentication.getPrincipal();
        } catch (DisabledException | LockedException | CredentialsExpiredException disabledException) {
            throw RestException.restThrow(MessageConstants.USER_NOT_FOUND_OR_DISABLED, HttpStatus.FORBIDDEN);
        } catch (BadCredentialsException | UsernameNotFoundException badCredentialsException) {
            throw RestException.restThrow(MessageConstants.LOGIN_OR_PASSWORD_ERROR, HttpStatus.FORBIDDEN);
        }

        LocalDateTime tokenIssuedAt = LocalDateTime.now();
        String accessToken = jwtTokenProvider.generateAccessToken(user, Timestamp.valueOf(tokenIssuedAt));
        String refreshToken = jwtTokenProvider.generateRefreshToken(user);

        user.setTokenIssuedAt(tokenIssuedAt);
        userRepository.save(user);

        TokenDTO tokenDTO = TokenDTO
                .builder()
                .accessToken(accessToken)
                .refreshToken(refreshToken)
                .build();

        return ApiResult.successResponse(
                tokenDTO,
                MessageConstants.SUCCESSFULLY_TOKEN_GENERATED);
    }

    @Override
    public ApiResult<TokenDTO> refreshToken(String accessToken, String refreshToken) {
        accessToken = accessToken.substring(accessToken.indexOf("Bearer") + 6).trim();
        try {
            jwtTokenProvider.checkToken(accessToken, true);
        } catch (ExpiredJwtException ex) {
            try {
                String userId = jwtTokenProvider.getUserIdFromToken(refreshToken, false);
                User user = getUserById(UUID.fromString(userId)).orElseThrow(() -> RestException.restThrow("Token not valid"));

                if (!user.isEnabled()
                        || !user.isAccountNonExpired()
                        || !user.isAccountNonLocked()
                        || !user.isCredentialsNonExpired())
                    throw RestException.restThrow(MessageConstants.USER_PERMISSION_RESTRICTION, HttpStatus.UNAUTHORIZED);

                LocalDateTime tokenIssuedAt = LocalDateTime.now();
                String newAccessToken = jwtTokenProvider.generateAccessToken(user, Timestamp.valueOf(tokenIssuedAt));
                String newRefreshToken = jwtTokenProvider.generateRefreshToken(user);

                user.setTokenIssuedAt(tokenIssuedAt);
                userRepository.save(user);

                TokenDTO tokenDTO = TokenDTO.builder()
                        .accessToken(newAccessToken)
                        .refreshToken(newRefreshToken)
                        .build();
                return ApiResult.successResponse(tokenDTO);
            } catch (Exception e) {
                throw RestException.restThrow(MessageConstants.REFRESH_TOKEN_EXPIRED, HttpStatus.UNAUTHORIZED);
            }
        } catch (Exception ex) {
            throw RestException.restThrow(MessageConstants.WRONG_ACCESS_TOKEN, HttpStatus.UNAUTHORIZED);
        }
        throw RestException.restThrow(MessageConstants.ACCESS_TOKEN_NOT_EXPIRED, HttpStatus.UNAUTHORIZED);
    }

    @Override
    public Optional<User> getUserById(UUID id) {
        return userRepository.findById(id);
    }


}
