package com.example.transaction3.service;

import com.example.transaction3.entity.User;
import com.example.transaction3.exceptions.RestException;
import com.example.transaction3.payload.CardAddDTO;
import com.example.transaction3.payload.ErrorData;
import com.example.transaction3.payload.SignDTO;
import com.example.transaction3.payload.VerificationDTO;
import com.example.transaction3.repository.UserRepository;
import com.example.transaction3.utils.MessageConstants;
import com.example.transaction3.utils.StringHelper;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Optional;


@RequiredArgsConstructor
@Component
public class ValidatorService {

    private final UserRepository userRepository;

    public List<ErrorData> validateUser(SignDTO user) {
        List<ErrorData> errors = new ArrayList<>();
        if (userRepository.existsByPhoneNumber(user.getPhoneNumber()))
            addError("PhoneNumber", MessageConstants.NOT_UNIQUE, errors);
        if (!StringHelper.isValidPassword(user.getPassword()))
            addError("Password", "Must contains at least 1 upper, 1 lower and 1 numeric character", errors);
        return errors;
    }

    public List<ErrorData> validateVerificationCode(VerificationDTO verificationDTO) {
        List<ErrorData> errors = new ArrayList<>();

        Optional<User> user = userRepository.findByPhoneNumber(verificationDTO.getPhoneNumber());

        if (user.isEmpty())
            addError("user", MessageConstants.NOT_FOUND, errors);
        else {
            if (user.get().isEnable()) {
                addError("user", MessageConstants.ALREADY_VERIFIED, errors);
            } else {
                User user1 = user.get();
                if (!Objects.equals(user1.getVerificationCode(), verificationDTO.getVerificationCode())) {
                    addError("verification code", MessageConstants.VERIFICATION_CODE_MISMATCH, errors);
                } else {
                    user1.setEnable(true);
                    userRepository.save(user1);
                }
            }
        }
        return errors;
    }

    private void addError(String fieldName, String error, List<ErrorData> list) {
        list.add(ErrorData.builder()
                .errorMsg(error)
                .fieldName(fieldName)
                .build());
    }

    public List<ErrorData> checkCardNumberIsValidOrThrew(String cardNumber) {
        List<ErrorData> errors = new ArrayList<>();
        if (cardNumber.length() != 16)
            addError("card number", "Must contains only 16 numbers", errors);

        if (!(cardNumber.startsWith("9860") || cardNumber.startsWith("4200")))
            addError("card number", "Must begin 9860 or 4200", errors);

        for (char ch : cardNumber.toCharArray()) {
            if (!Character.isDigit(ch)){
                addError("card number", "Must contains only 16 numbers", errors);
                break;
            }
        }
        return errors;
    }
}
