package com.example.transaction3.service;

import com.example.transaction3.payload.ApiResult;
import com.example.transaction3.payload.CardAddDTO;
import com.example.transaction3.payload.CardDTO;
import com.example.transaction3.payload.VerificationDTO;

import java.util.List;

public interface CardService {
    ApiResult<CardDTO> add(CardAddDTO cardDTO);

    ApiResult<List<CardDTO>> getAll();

    ApiResult<CardDTO> get(String cardNumber);
}
