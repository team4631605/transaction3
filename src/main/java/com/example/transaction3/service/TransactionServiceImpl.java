package com.example.transaction3.service;

import com.example.transaction3.entity.Card;
import com.example.transaction3.entity.Rate;
import com.example.transaction3.entity.Transaction;
import com.example.transaction3.entity.User;
import com.example.transaction3.entity.enums.CardTypeEnum;
import com.example.transaction3.entity.enums.StatusEnum;
import com.example.transaction3.exceptions.RestException;
import com.example.transaction3.payload.ApiResult;
import com.example.transaction3.payload.TransactionDTO;
import com.example.transaction3.payload.TransactionHoldDTO;
import com.example.transaction3.repository.CardRepository;
import com.example.transaction3.repository.RateRepository;
import com.example.transaction3.repository.TransactionRepository;
import com.example.transaction3.repository.UserRepository;
import com.example.transaction3.utils.CommonUtils;
import com.example.transaction3.utils.MessageConstants;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Transactional;

import java.util.Objects;
import java.util.UUID;
@Service
@RequiredArgsConstructor
public class TransactionServiceImpl implements TransactionService{
    private final TransactionRepository transactionRepository;
    private final UserRepository userRepository;
    private final RateRepository rateRepository;
    private final CardRepository cardRepository;
    @Override
    @Transactional(isolation = Isolation.SERIALIZABLE)
    public ApiResult<TransactionDTO> hold(TransactionHoldDTO transactionHoldDTO) {
        if (Objects.equals(transactionHoldDTO.getReceiverCardId(),transactionHoldDTO.getSenderCardId()))
            throw RestException.restThrow(MessageConstants.RECEIVER_AND_SENDER_CARDS_ARE_SAME,HttpStatus.BAD_REQUEST);

        Transaction transaction =  checkAndmapDtoToTransaction(transactionHoldDTO);

        transaction = transactionRepository.save(transaction);

        return ApiResult.successResponse(mapTransactionToDTO(transaction));
    }

    @Override
    @Transactional(isolation = Isolation.SERIALIZABLE)
    public ApiResult<?> confirm(UUID transactionId) {
        User currentUser = CommonUtils.getCurrentUser();

        Transaction transaction = transactionRepository.findById(transactionId).orElseThrow(
                () -> RestException.restThrow(MessageConstants.TRANSACTION_NOT_FOUND, HttpStatus.NOT_FOUND));

        if (!cardRepository.existsByCardNumberAndUserId(transaction.getSenderCard().getCardNumber(),currentUser.getId()))
            throw RestException.restThrow(MessageConstants.NOT_ACCESS_TO_TRANSACTION , HttpStatus.FORBIDDEN);

        if (transaction.getSenderCard().getBalance() >= transaction.getSenderAmount()) {

            transaction.setStatus(StatusEnum.SUCCESS);
            Card senderCard = transaction.getSenderCard();
            Card receiverCard = transaction.getReceiverCard();

            senderCard.setBalance(senderCard.getBalance()-transaction.getSenderAmount());
            receiverCard.setBalance(receiverCard.getBalance()+transaction.getReceiverAmount());

            cardRepository.save(senderCard);
            cardRepository.save(receiverCard);
            transactionRepository.save(transaction);
            return ApiResult.successResponse(MessageConstants.TRANSACTION_SUCCESSFULLY_COMPLETED);
        }

        transaction.setStatus(StatusEnum.ERROR);
        return ApiResult.errorResponse(MessageConstants.TRANSACTION_FAILED,HttpStatus.NOT_ACCEPTABLE.value());
    }




    private Transaction checkAndmapDtoToTransaction(TransactionHoldDTO transactionHoldDTO) {
        User currentUser = CommonUtils.getCurrentUser();

        Card senderCard = cardRepository.findById(transactionHoldDTO.getSenderCardId()).orElseThrow(
                () -> RestException.restThrow(MessageConstants.SENDER_CARD_NUMBER_NOT_FOUND, HttpStatus.NOT_FOUND));
        Card receiverCard = cardRepository.findById(transactionHoldDTO.getReceiverCardId()).orElseThrow(
                () -> RestException.restThrow(MessageConstants.RECEIVER_CARD_NUMBER_NOT_FOUND, HttpStatus.NOT_FOUND));

        Rate rate = rateRepository.findByFromCurrencyAndToCurrency(senderCard.getCurrency(), receiverCard.getCurrency())
                .orElseThrow(() -> RestException.restThrow(MessageConstants.RATE_NOT_FOUND, HttpStatus.NOT_FOUND));

        checkSenderCardValidOrElseThrew(currentUser , senderCard);
        checkCardBalance(transactionHoldDTO, senderCard);

        long receiverAmount, senderAmount;
        if (rate.getRate() < 0){
            long rateAmount = Math.abs(rate.getRate());
            receiverAmount = transactionHoldDTO.getAmount() / rateAmount;
            if (receiverAmount==0)
                throw RestException.restThrow(MessageConstants.INSUFFICIANT_FUNDS, HttpStatus.BAD_REQUEST);
            senderAmount = receiverAmount*rateAmount;
        } else {
            receiverAmount = transactionHoldDTO.getAmount() * rate.getRate();
            senderAmount = transactionHoldDTO.getAmount();
        }

        return Transaction.builder()
                .senderCard(senderCard)
                .senderAmount(senderAmount)
                .receiverCard(receiverCard)
                .receiverAmount(receiverAmount)
                .status(StatusEnum.NEW)
                .time(System.currentTimeMillis())
                .build();
    }

    private TransactionDTO mapTransactionToDTO(Transaction transaction) {
        return TransactionDTO.builder()
                .id(transaction.getId())
                .senderCardId(transaction.getSenderCard().getId())
                .receiverCardId(transaction.getReceiverCard().getId())
                .senderAmount(transaction.getSenderAmount())
                .receiverAmount(transaction.getReceiverAmount())
                .senderCurrency(transaction.getSenderCard().getCurrency())
                .receiverCurrency(transaction.getReceiverCard().getCurrency())
                .build();
    }

    private void checkCardBalance(TransactionHoldDTO transactionHoldDTO, Card senderCard) {
        if (senderCard.getBalance() < transactionHoldDTO.getAmount())
            throw RestException.restThrow(MessageConstants.INSUFFICIANT_FUNDS, HttpStatus.BAD_REQUEST);
    }

    private void checkSenderCardValidOrElseThrew(User currentUser, Card senderCard) {
        if (!Objects.equals(currentUser.getId(),senderCard.getUser().getId()))
            throw RestException.restThrow(MessageConstants.CARD_NOT_RELATED_TO_USER, HttpStatus.BAD_REQUEST);
    }
}
