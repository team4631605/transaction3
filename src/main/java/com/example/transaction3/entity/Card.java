package com.example.transaction3.entity;

import com.example.transaction3.entity.enums.CardTypeEnum;
import com.example.transaction3.entity.enums.CurrencyEnum;
import com.example.transaction3.entity.template.AbsLongEntity;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.validation.constraints.Min;
import javax.validation.constraints.Size;

import lombok.*;

@Entity
@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class Card extends AbsLongEntity {

    @Column(nullable = false)
    @Min(0)

    private Long balance;

    @Column(nullable = false)
    private String name;

    @Column(nullable = false, unique = true)
    @Size(min=16, max=16)
    private String cardNumber;

    @Column(nullable = false)
    private CardTypeEnum type;

    @Column(nullable = false)
    private CurrencyEnum currency;

    @ManyToOne(optional = false)
    private User user;

    private boolean deleted;
}
