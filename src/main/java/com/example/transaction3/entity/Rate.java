package com.example.transaction3.entity;

import com.example.transaction3.entity.enums.CurrencyEnum;
import com.example.transaction3.entity.template.AbsLongEntity;
import javax.persistence.Column;
import javax.persistence.Entity;
import lombok.*;

@Entity
@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class Rate extends AbsLongEntity {
    @Column(nullable = false)
    private CurrencyEnum fromCurrency;

    @Column(nullable = false)
    private CurrencyEnum toCurrency;

    @Column(nullable = false)
    private Long rate;

    private boolean deleted;

}
