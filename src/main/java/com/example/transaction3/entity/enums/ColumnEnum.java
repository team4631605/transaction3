package com.example.transaction3.entity.enums;

public enum ColumnEnum {
    CATEGORY_NAME,
    CATEGORY_DESC,
    RATE_NAME,
    RATE_DESC,
    PLANE_NAME,
    PLANE_DESC,
    AMENITY_NAME,
    AMENITY_DESC,
    HOTEL_NAME,
    HOTEL_DESC

}
