package com.example.transaction3.entity.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;
import org.springframework.security.core.GrantedAuthority;

@AllArgsConstructor
@Getter
public enum PermissionEnum  implements GrantedAuthority{
    ADD_CARD, GET_USER_CARDS, GET_ALL_USER_CARDS, GET_USER_CARD, HOLD_TRANSACTION, CONFIRM_TRANSACTION;

    @Override
    public String getAuthority() {
        return name();
    }
}
