package com.example.transaction3.entity;


import com.example.transaction3.entity.enums.PermissionEnum;
import com.example.transaction3.entity.template.AbsIntegerEntity;
import javax.persistence.*;
import lombok.*;

import java.util.Set;

@Entity
@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class Role extends AbsIntegerEntity {

    @Column(unique = true, nullable = false)
    private String name;

    @Column(length = 500)
    private String description;

    @CollectionTable(name = "role_permission",
            joinColumns =
            @JoinColumn(name = "role_id", referencedColumnName = "id"))
    @ElementCollection
    @Enumerated(EnumType.STRING)
    @Column(name = "permission", nullable = false)
    private Set<PermissionEnum> permissions;

    private boolean deleted;

}
