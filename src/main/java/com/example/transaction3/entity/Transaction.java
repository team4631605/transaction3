package com.example.transaction3.entity;

import com.example.transaction3.entity.enums.StatusEnum;
import com.example.transaction3.entity.template.AbsUUIDEntity;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ManyToOne;
import lombok.*;

@Entity
@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class Transaction extends AbsUUIDEntity {

    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    private Card senderCard;
    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    private Card receiverCard;
    @Column(nullable = false)
    private Long senderAmount;
    @Column(nullable = false)
    private Long receiverAmount;
    @Column(nullable = false)
    private StatusEnum status;
    @Column(nullable = false)
    private Long time;
    private boolean deleted;
}
